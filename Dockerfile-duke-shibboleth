FROM image-mirror-prod-registry.cloud.duke.edu/library/centos:centos7

LABEL name 'duke-shibboleth'
LABEL version 'centos7'
LABEL release '1.0.1'
LABEL vendor 'Duke University, Office of Information Technology, Automation'
LABEL URL 'https://cloud.duke.edu'
LABEL architecture 'x86_64'
LABEL summary 'OIT Systems Shibboleth image'
LABEL description 'OIT Systems Shiboleth image'
LABEL distribution-scope 'public'
LABEL authoritative-source-url 'https://gitlab.oit.duke.edu/devil_ops/duke-shibboleth-contianer'
LABEL changelog-url 'https://gitlab.oit.duke.edu/devil_ops/duke-shibboleth-contianer/activity'

# twistlock scan prevention
LABEL twistlockenv=production

ARG CI_COMMIT_SHA=unspecified
LABEL git_commit=${CI_COMMIT_SHA}

ARG CI_PROJECT_URL=unspecified
LABEL git_repository_url=${CI_PROJECT_URL}

ENV SHIBURL='https://shib.oit.duke.edu' \
    IDPXML='duke-metadata-2-signed.xml' \
    IDPCRT='idp_signing.crt'

# Need to set so Shib knows where to look for libcurl stuff
ENV LD_LIBRARY_PATH='/opt/shibboleth/lib64'


ADD shibboleth-oit.repo /etc/yum.repos.d/shibboleth-oit.repo

RUN yum install -y -q --nogpgcheck shibboleth \
    bind-license \
    libssh2 \
    python \
    python-libs \
    && yum update --nogpgcheck -y \
    && yum clean all \
    && rm -rf /var/cache/yum

RUN sed -i 's|var/log/shibboleth.*|/proc/self/fd/1|' /etc/shibboleth/shibd.logger

# Copy from https://shib.oit.duke.edu
ADD ${SHIBURL}/${IDPXML} /etc/shibboleth/${IDPXML}
ADD ${SHIBURL}/${IDPCRT} /etc/shibboleth/${IDPCRT}

RUN chown -R 1000 /etc/shibboleth
RUN chgrp -R root /etc/shibboleth
RUN chmod -R g+r /etc/shibboleth

RUN chown -R 1000 /var/run/shibboleth
RUN chgrp root /var/run/shibboleth
RUN chmod g+rw /var/run/shibboleth

RUN chown -R 1000 /var/cache/shibboleth
RUN chgrp root /var/cache/shibboleth
RUN chmod g+rw /var/cache/shibboleth

USER 1000
CMD ["/usr/sbin/shibd", "-F", "-f", "-w 30"]
